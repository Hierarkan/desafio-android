package com.desafio;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Author: filipeabreu
 * Created: 9/14/16 - 21:43
 * Project: Getninjas
 * Packge: tools
 */
public class Globals {

    public static Activity instance = null;
    public static void setActivityInstance(Activity newActivity){
        instance = newActivity;
    }
    public static Activity getActivityInstance()  {
        return instance ;
    }

    public static ProgressDialog loadDialog = null;
    public static void setDialogAtual(ProgressDialog dialogAberto){
        loadDialog = dialogAberto;
    }
    public static ProgressDialog getDialogAtual()  {
        return loadDialog ;
    }

    public static String telaNome;
    public static void setTelaNome(String telaNome) {
        Globals.telaNome = telaNome;
    }
    public static String getTelaNome() {
        return telaNome;
    }

    // Box com menssagem de carregamento
    public static void carregando(String msg) {
        Globals.setDialogAtual(new ProgressDialog(Globals.getActivityInstance()));
        Globals.getDialogAtual().setMessage(msg);
        Globals.getDialogAtual().setProgressStyle(ProgressDialog.STYLE_SPINNER);
        Globals.getDialogAtual().setIndeterminate(true);
        Globals.getDialogAtual().setCancelable(false);
        Globals.getDialogAtual().show();
    }
    // Box com menssagem de erro
    public static void avisoErro(int msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Globals.getActivityInstance());
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public static boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) Globals.getActivityInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
