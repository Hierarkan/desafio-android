package com.desafio;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    public static RecyclerView mRecyclerView;
    public LinearLayoutManager mLayoutManager;
    public static gitRepositorio_Adapater mAdapter;
    public static List<gitRepositorio> repositorioLista = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Globals.setActivityInstance(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if( !ImageLoader.getInstance().isInited()){
            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.ARGB_8888)
                    .build();
            ImageLoaderConfiguration configPersons;
            configPersons = new ImageLoaderConfiguration.Builder(Globals.getActivityInstance().getApplicationContext())
                    .defaultDisplayImageOptions(defaultOptions)
                    .build();
            ImageLoader.getInstance().init(configPersons);
        }
        repositorioLista = new ArrayList<>();
        mAdapter = new gitRepositorio_Adapater(repositorioLista);
        mRecyclerView = (RecyclerView) findViewById(R.id.main_recycler_view);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addOnScrollListener(new EndlessScrollListener_Repositorio(mLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                int lastFirstVisiblePosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                mRecyclerView.getLayoutManager().scrollToPosition(lastFirstVisiblePosition);
                updateList(String.valueOf(page));
            }
        });

        if (Globals.isNetworkAvailable()){
            verificarpermisoes();
        } else {
            Globals.avisoErro(R.string.error_internet);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Globals.setActivityInstance(this);
    }

    public static void mostrarRepo(gitRepositorio_Interface listaRepo) {

        for(int i=0; i < listaRepo.gitRepositorio.size(); i++ ){
            gitRepositorio item = new gitRepositorio();
            item.setName(listaRepo.gitRepositorio.get(i).getName());
            item.setDescription(listaRepo.gitRepositorio.get(i).getDescription());
            item.setForks(listaRepo.gitRepositorio.get(i).getForks());
            item.setStar(listaRepo.gitRepositorio.get(i).getStar());
            item.setAutor(listaRepo.gitRepositorio.get(i).getAutor());
            item.setPulls_url(listaRepo.gitRepositorio.get(i).getPulls_url());
            repositorioLista.add(item);
        }
        mAdapter.notifyDataSetChanged();
        Globals.getDialogAtual().dismiss();
    }

    public void updateList(String pagina) {
        Globals.carregando("Carregando...");
        gitAPI API = new gitAPI();
        try {
            API.lista("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + pagina, "repo");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Permisoes do aplicativo
    public void verificarpermisoes(){
        // permissoes necessarias para rodar o app
        if (    ContextCompat.checkSelfPermission(this, android.Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            // box basica sem explicacao
            ActivityCompat.requestPermissions(Globals.getActivityInstance(),
                    new String[]{android.Manifest.permission.INTERNET,
                            android.Manifest.permission.ACCESS_NETWORK_STATE}, 1);
        } else {
            try {
                updateList("1");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // retorno das permissoes
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (    grantResults.length == 2 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED ) {
                    try {
                        updateList("1");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    MainActivity.this.finish();
                }
                break;
        }
    }
}
