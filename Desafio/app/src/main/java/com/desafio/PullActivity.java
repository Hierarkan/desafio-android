package com.desafio;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class PullActivity extends AppCompatActivity {

    public static RecyclerView mRecyclerView;
    public LinearLayoutManager mLayoutManager;
    public static gitPull_Adapater mAdapter;
    public static List<gitPull> pullLista = new ArrayList<>();
    public static TextView pull_vazio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);

        Globals.setActivityInstance(this);

        Globals.carregando("Carregando...");

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle(Globals.getTelaNome());

        pull_vazio = (TextView) findViewById(R.id.pull_vazio);

        pullLista = new ArrayList<>();
        mAdapter = new gitPull_Adapater(pullLista);
        mRecyclerView = (RecyclerView) findViewById(R.id.pull_recycler_view);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(this);
    }

    public static void mostrarPull(gitPull[] listapull) {

        if (listapull == null || listapull.length == 0){
            pull_vazio.setVisibility(View.VISIBLE);
        } else {
            for (gitPull aListapull : listapull) {
                gitPull item = new gitPull();
                item.setTitle(aListapull.getTitle());
                item.setBody(aListapull.getBody());
                item.setHtml_url(aListapull.getHtml_url());
                item.setCreated_at(aListapull.getCreated_at());
                item.setAutor(aListapull.getAutor());
                pullLista.add(item);
            }
            mAdapter.notifyDataSetChanged();
        }
        Globals.getDialogAtual().dismiss();
    }
}
