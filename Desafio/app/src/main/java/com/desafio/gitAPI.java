package com.desafio;

import android.os.Handler;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Author: filipeabreu
 * Created: 9/14/16 - 21:43
 * Project: Getninjas
 * Packge: tools
 */
public class gitAPI {

    public void lista( String url, String action) throws Exception {
        rest(url, action);
    }

    public void rest(String url, final String action){
        System.out.println(url);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            Handler mainHandler = new Handler(Globals.getActivityInstance().getMainLooper());
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (action.equals("repo")){
                                gitRepositorio_Interface listaRepo = gitRepositorio_Interface.parseJSON(response.body().string());
                                MainActivity.mostrarRepo(listaRepo);
                            }
                            if (action.equals("pull")){
                                PullActivity.mostrarPull(gitPull_Interface.parseJSON(response.body().string()));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
