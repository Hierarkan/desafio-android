package com.desafio;

import com.google.gson.annotations.SerializedName;

/**
 * Author: filipeabreu
 * Created: 9/23/16 - 18:53
 * Project: Desafio
 * Packge: com.desafio
 */
public class gitPull {

    @SerializedName("title")
    String title;
    @SerializedName("body")
    String body;
    @SerializedName("html_url")
    String html_url;
    @SerializedName("created_at")
    String created_at;
    @SerializedName("user")
    gitPullAutor autor;


    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setBody(String body) {
        this.body = body;
    }
    public String getBody() {
        return body;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }
    public String getHtml_url() {
        return html_url;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    public String getCreated_at() {
        return created_at;
    }

    public void setAutor(gitPullAutor autor) {
        this.autor = autor;
    }
    public gitPullAutor getAutor() {
        return autor;
    }
}

class gitPullAutor {

    @SerializedName("login")
    String login;
    @SerializedName("avatar_url")
    String avatar_url;


    public String getLogin() {
        return login;
    }
    public String getAvatar_url() {
        return avatar_url;
    }
}
