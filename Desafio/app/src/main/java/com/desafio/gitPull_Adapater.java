package com.desafio;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Author: filipeabreu
 * Created: 9/23/16 - 19:06
 * Project: Desafio
 * Packge: com.desafio
 */
public class gitPull_Adapater extends RecyclerView.Adapter<gitPull_ViewHolder> {

    private List<gitPull> listItemsList;

    public gitPull_Adapater(List<gitPull> listItemsList) {
        this.listItemsList = listItemsList;
    }

    @Override
    public gitPull_ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pull, null);
        return new gitPull_ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final gitPull_ViewHolder gitPullViewHolder, final int position) {

        gitPull listItems = listItemsList.get(position);
        int focusedItem = 0;
        gitPullViewHolder.itemView.setSelected(focusedItem == position);

        gitPullViewHolder.getLayoutPosition();

        gitPullViewHolder.pull_name.setText(listItems.getTitle());
        gitPullViewHolder.pull_desc.setText(listItems.getBody());
        gitPullViewHolder.pull_autor_name.setText(listItems.getAutor().getLogin());

        String data = listItems.getCreated_at().substring(8, 10) + "/";
        data += listItems.getCreated_at().substring(5, 7) + "/";
        data += listItems.getCreated_at().substring(0, 4);
        gitPullViewHolder.pull_autor_data.setText(data);

        ImageLoader.getInstance().displayImage(listItems.getAutor().getAvatar_url(), gitPullViewHolder.pull_avatar);

        gitPullViewHolder.pull_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Globals.getActivityInstance().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.stackoverflow.com")));
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != listItemsList ? listItemsList.size() : 0);
    }

}