package com.desafio;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Author: filipeabreu
 * Created: 9/23/16 - 18:59
 * Project: Desafio
 * Packge: com.desafio
 */
public class gitPull_Interface {

    @SerializedName("items")
    List<gitPull> gitPull;

    public gitPull_Interface() {
        gitPull = new ArrayList<>();
    }

    public static com.desafio.gitPull[] parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        Type collectionType = new TypeToken<Collection<gitPull>>() {}.getType();
        Collection<gitPull> enuns = gson.fromJson(response, collectionType);
        return enuns.toArray(new gitPull[enuns.size()]);
    }
}
