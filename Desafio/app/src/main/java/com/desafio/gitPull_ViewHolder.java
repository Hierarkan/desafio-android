package com.desafio;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Author: filipeabreu
 * Created: 9/23/16 - 19:07
 * Project: Desafio
 * Packge: com.desafio
 */
public class gitPull_ViewHolder extends RecyclerView.ViewHolder {
    public TextView pull_name, pull_desc, pull_autor_name, pull_autor_data;
    public ImageView pull_avatar;
    public RelativeLayout pull_item;

    public gitPull_ViewHolder(View view) {
        super(view);
        this.pull_item          = (RelativeLayout) view.findViewById(R.id.pull_item);
        this.pull_name          = (TextView) view.findViewById(R.id.pull_name);
        this.pull_desc          = (TextView) view.findViewById(R.id.pull_desc);
        this.pull_autor_name    = (TextView) view.findViewById(R.id.pull_autor_name);
        this.pull_autor_data    = (TextView) view.findViewById(R.id.pull_autor_data);
        this.pull_avatar        = (ImageView) view.findViewById(R.id.pull_avatar);

    }
}

