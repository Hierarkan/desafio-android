package com.desafio;

import com.google.gson.annotations.SerializedName;

/**
 * Author: filipeabreu
 * Created: 9/23/16 - 01:38
 * Project: Desafio
 * Packge: com.desafio
 */
public class gitRepositorio {

    @SerializedName("name")
    String name;
    @SerializedName("description")
    String description;
    @SerializedName("forks")
    String forks;
    @SerializedName("stargazers_count")
    String star;
    @SerializedName("owner")
    gitAutor autor;
    @SerializedName("pulls_url")
    String pulls_url;

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public void setForks(String forks) {
        this.forks = forks;
    }
    public String getForks() {
        return forks;
    }

    public void setStar(String star) {
        this.star = star;
    }
    public String getStar() {
        return star;
    }

    public void setAutor(gitAutor autor) {
        this.autor = autor;
    }
    public gitAutor getAutor() {
        return autor;
    }

    public void setPulls_url(String pulls_url) {
        this.pulls_url = pulls_url;
    }
    public String getPulls_url() {
        return pulls_url;
    }
}

class gitAutor {

    @SerializedName("login")
    String login;
    @SerializedName("type")
    String type;
    @SerializedName("avatar_url")
    String avatar_url;


    public String getLogin() {
        return login;
    }
    public String getType() {
        return type;
    }
    public String getAvatar_url() {
        return avatar_url;
    }
}
