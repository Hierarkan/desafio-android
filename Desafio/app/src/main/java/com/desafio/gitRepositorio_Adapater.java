package com.desafio;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Author: filipeabreu
 * Created: 9/23/16 - 13:27
 * Project: Desafio
 * Packge: com.desafio
 */
public class gitRepositorio_Adapater extends RecyclerView.Adapter<gitRepositorio_ViewHolder> {

    private List<gitRepositorio> listItemsList;

    public gitRepositorio_Adapater(List<gitRepositorio> listItemsList) {
        this.listItemsList = listItemsList;
    }

    @Override
    public gitRepositorio_ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int position) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_repositorio, null);
        return new gitRepositorio_ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final gitRepositorio_ViewHolder gitRepositorioViewHolder, final int position) {

        final gitRepositorio listItems = listItemsList.get(position);
        int focusedItem = 0;
        gitRepositorioViewHolder.itemView.setSelected(focusedItem == position);

        gitRepositorioViewHolder.getLayoutPosition();

        gitRepositorioViewHolder.repo_name.setText(listItems.getName());
        gitRepositorioViewHolder.repo_desc.setText(listItems.getDescription());
        gitRepositorioViewHolder.repo_forks.setText(listItems.getForks());
        gitRepositorioViewHolder.repo_star.setText(listItems.getStar());
        gitRepositorioViewHolder.autor_name.setText(listItems.getAutor().getLogin());
        gitRepositorioViewHolder.autor_org.setText(listItems.getAutor().getType());

        ImageLoader.getInstance().displayImage(listItems.getAutor().getAvatar_url(), gitRepositorioViewHolder.avatar);

        gitRepositorioViewHolder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            try {
                Globals.setTelaNome(listItems.getName());
                Intent itemDetalhe = new Intent();
                itemDetalhe.setClass(Globals.getActivityInstance(), PullActivity.class);
                Globals.getActivityInstance().startActivity(itemDetalhe);
                gitAPI API = new gitAPI();
                API.rest(listItems.getPulls_url().replace("{/number}",""), "pull");
            } catch (Exception e) {
                e.printStackTrace();
            }
            }
        });
    }

    public void clearAdapter() {
        listItemsList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (null != listItemsList ? listItemsList.size() : 0);
    }

}