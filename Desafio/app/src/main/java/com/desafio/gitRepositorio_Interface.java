package com.desafio;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: filipeabreu
 * Created: 9/23/16 - 01:40
 * Project: Desafio
 * Packge: com.desafio
 */
public class gitRepositorio_Interface {

    @SerializedName("items")
    List<gitRepositorio> gitRepositorio;

    public gitRepositorio_Interface() {
        gitRepositorio = new ArrayList<>();
    }

    public static gitRepositorio_Interface parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(response, gitRepositorio_Interface.class);
    }
}
