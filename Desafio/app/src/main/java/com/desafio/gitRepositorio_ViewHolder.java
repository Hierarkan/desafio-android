package com.desafio;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Author: filipeabreu
 * Created: 9/23/16 - 15:56
 * Project: Desafio
 * Packge: com.desafio
 */
public class gitRepositorio_ViewHolder extends RecyclerView.ViewHolder {
    public TextView repo_name, repo_desc, repo_forks, repo_star, autor_name, autor_org;
    public ImageView avatar;
    public RelativeLayout item;

    public gitRepositorio_ViewHolder(View view) {
        super(view);
        this.item      = (RelativeLayout) view.findViewById(R.id.item);
        this.repo_name   = (TextView) view.findViewById(R.id.repo_name);
        this.repo_desc   = (TextView) view.findViewById(R.id.repo_desc);
        this.repo_forks  = (TextView) view.findViewById(R.id.repo_forks);
        this.repo_star   = (TextView) view.findViewById(R.id.repo_star);
        this.autor_name  = (TextView) view.findViewById(R.id.autor_name);
        this.autor_org   = (TextView) view.findViewById(R.id.autor_org);

        this.avatar      = (ImageView) view.findViewById(R.id.avatar);
    }
}

